package fr.epita.finalProject.dataModel;

/**
 * Datamodel Class: Represents a List entity from the Task Manager.
 * @author Emmanuel Bentum & Fabrice Prince
 */
public class Lists {
	/*
	 * ########### CLASS ATTRIBUTES ###########
	 */
	/**
	 * Attribute: A String representing the 'ListName' of a 'List'.
	 */
	private String listName;

	/*
	 * ############ CONSTRUCTOR ############
	 */
	/**
	 * Constructor: Creates an instance of 'List'
	 * @param listName A String representing the 'ListName' of a 'List'.
	 */
	public Lists(String listName) {
		this.listName = listName;
	}

	/*
	 * ############ GETTERS AND SETTERS ############
	 */
	/**
	 * Getter: Returns the attribute 'listName' of a List
	 * @return A String representing the 'listName' of a 'List'
	 */
	public String getListsName() {
		return listName;
	}

	/**
	 * Setter: Updates the attribute 'listName' of a List
	 * @param listName A String representing the 'ListName' of a 'List'
	 */
	public void setListsName(String listName) {
		this.listName = listName;
	}
}
