package fr.epita.finalProject.dataModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.epita.finalProject.services.*;

/**
 * Datamodel Class: Emulates the main features of an online Task manager "Trello".
 * Provides a set of methods to operate CRUD on the task manager entities.
 * @author Emmanuel Bentum, Fabrice Prince 
 */
public class epitrelloDataService  {
	/*
	 * ########### CLASS ATTRIBUTES ###########
	 */
	/**
	 * Attribute: A {@code List<User>} representing the list of {@code User} of the Task Manager.
	 */
	List<User> userList       = new ArrayList<User>();
	/**
	 * Attribute: A {@code List<Lists>} representing the list of {@code Lists} of the Task Manager.
	 */
	List<Lists> listOfLists   = new ArrayList<Lists>();
	/**
	 * Attribute: A {@code List<Task>} representing the list of {@code Task} of the Task Manager.
	 */
	List<Task> taskList       = new ArrayList<Task>();
	
	/**
	 * Attribute: A {@code List<Task>} representing the list of unassigned {@code Task} of the Task Manager.
	 */
	List<Task> unassignedTask = new ArrayList<Task>();
	/**
	 * Attribute: A {@code List<Task>} representing the list of assigned {@code Task} of the Task Manager.
	 */
	List<Task> assignedTask   = new ArrayList<Task>();
	/**
	 * Attribute: A {@code List<Task>} representing the list of completed {@code Task} of the Task Manager.
	 */
	List<Task> completedTask  = new ArrayList<Task>(); 
	/**
	 * Attribute: A {@code Map<String, List<String>>} representing a dictionary of {@code User} and their {@code Task} in the Task Manager.
	 */
	Map<String, List<String>> usersTasks = new HashMap<String,List<String>>();
	
	DatabaseDAO userDao = new DatabaseDAO();
	
	private TextFileDAO fichier;
	private String msg="";
	
	/*
	 * ############ CONSTRUCTOR ############
	 */
	/**
	 * Constructor: Creates an instance of {@code epitrelloDataService}.
	 * @throws ClassNotFoundException 
	 */
	public epitrelloDataService () throws ClassNotFoundException {
		this.userList = userDao.searchUsers();
		fichier = new TextFileDAO();
	}	
	
	/*
	 * ############ METHODS FOR USERS ############
	 */
	/**
	 * Method: Allows to create a new 'User' entity.
	 * Returns "AddUser-Success" if successful,
	 * 		   "AddUser Failed: User already exists" otherwise
	 * @param  name A String representing the 'name' of a 'User'
	 * @return A String representing the success/failure of the operation
	 * @throws ClassNotFoundException 
	 */
	public String addUser(String name) throws ClassNotFoundException {
		int found = 0;
		String msg="";
		Iterator<User> userListIterator = userList.iterator();
		
		// First, check if user already exists
		while(userListIterator.hasNext()) {
			if ( userListIterator.next().getName().equals(name) ) 
				found = 1;
		}
		// If not, create a new User and return "success" message
		if (found == 0) {
			User utilisateur = new User(name);
			userList.add(utilisateur);
			// Add User to the DB
			userDao.createUser(utilisateur);
			 msg="AddUser-Success";
			fichier.write(msg);
			return msg;
		}
		//  Else, a message "Already exists" is returned
		else {
			
			return "AddUser Failed: User already exists";
		}
	}
	
	/**
	 * Method: Allows to display 'Users' sorted by their performance
	 * @return A String corresponding to Users sorted by performance
	 */
	public String printUsersByPerformance() {
		
		int sum = 0;
		StringBuilder sb = new StringBuilder();
		
		// the total amount of estimatedTime of its tasks
		for ( Map.Entry<String,List<String>> map : usersTasks.entrySet()) {
			sum = 0;
			
			for (String s: map.getValue()) { // iterate the tasks of a user
				
				for(Task t: taskList) {
					if (t.getName().equals(s)) {
						sum += t.getEstimatedTime();
					}
				}
			}
			sb.append (map.getKey() + "'s performance is " + sum + "\n");	
		}
		fichier.write(sb.toString());
		return sb.toString();
	}
	
	/**
	 * 
	 * @return a String that represent the user with his workloads 
	 */
	public String printUsersByWorkload() {
	
		StringBuilder sb = new StringBuilder();
		// the number of tasks it has 
		
		for ( Map.Entry<String,List<String>> map : usersTasks.entrySet()) {
			
			sb.append(map.getKey() +" has " + map.getValue().size() +" task(s)\n");
		}
		
		fichier.write(sb.toString());
		return sb.toString();
	}
	
		
	/*
	 * ############ METHODS FOR LISTS ############
	 */
	/**
	 * 
	 * @param listName
	 * @return a String saying the addList  succeeded or not 
	 */
	public String addList(String listName) {
		
		int found = 0;
		Iterator<Lists> listIterator = listOfLists.iterator();
		
		while(listIterator.hasNext()) {
			if (listIterator.next().getListsName().equals(listName) ) 
				found=1;
		}
		
		if ( found ==0 ) {
			Lists list = new Lists(listName);
			listOfLists.add(list);
			msg="AddList-Success";
			fichier.write(msg);
			return msg;
		}
		else
			return "AddList Failed: List already exists";
	}
	
	//------------------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * 
	 * @param listName represents the name of the list
	 * @return  a String saying the process succeeded or not
	 */
	public String deleteList(String listName) {
		
		Iterator<Lists> listIterator = listOfLists.iterator();
		int del=0;
		while (listIterator.hasNext()) {
			if (listIterator.next().getListsName().equals(listName))
			{
				listIterator.remove();
				del=1;
			}
		}
		if (del == 1 ) {
			msg="DeleteList: Success";
			return msg;
		}
		else
			return "Delete List Failed: The list name specified does not exist";
			
		}
	
	//------------------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * 
	 * @param name representing the name of a list
	 * @return a String representing the information of a list passed by argument
	 */
	public String printList(String name) {
		Iterator<Lists> iterList=listOfLists.iterator();
		while(iterList.hasNext()) {
			if (iterList.next().equals(name))
				
				msg= " List name : "+ iterList.next().getListsName();
				fichier.write(msg);
				return msg;
		}
		
		return "List not found";
	}

	//------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * 
	 * @return  a String which represent all the list in the system
	 */
	public String printAllLists() {
		
		int k=0;
		StringBuilder sb = new StringBuilder();
		Iterator<Lists> iterList=listOfLists.iterator();
		while(iterList.hasNext()) {
			k++;
			sb.append("List "+ iterList.next().getListsName() +"\n");
		}
		
		fichier.write(sb.toString());
		return  sb.toString();
	}
	
	

	

	
	/*
	 * ############ METHODS FOR TASKS ############
	 */	
	
	/**
	 * 
	 * @param list: is the name of the list the task belonged to
	 * @param name : is the name of the tasks
	 * @param estimatedTime : is the estimated time to accomplish a task 
	 * @param priority  is a number representing the priority of a task
	 * @param description is a text describing the task
	 * @return a string that says either it succeeded or not
	 */
	public String addTask(String list, String name,  int estimatedTime, int priority, String description) {
		
		/*
		 * First , check if Task & list specified exists 
		 * if yes a message " is returned 
		 * else we create a new user 
		 */
		int foundTask=0 , foundList=0;
		
		// we create Iterators so that we can explore the list of Objects Task
		Iterator<Task> taskIterator  = taskList.iterator();
		Iterator<Lists> listIterator = listOfLists.iterator();
		
		while(taskIterator.hasNext()) {
			/* check if the user name passed by parameter is equals
			 * to the name  of the tasks in the list 
			 */
			if (taskIterator.next().getName().equals(name) ) 
				
				foundTask=1;  // means the "name" specified is found
		}
		 // we do the same as previous with the "list" passed by parameter
		while (listIterator.hasNext()) {
			if (listIterator.next().getListsName().equals(list))
				foundList=1; // means the list specified exist
		}
		
		if (foundTask == 1 ) 
			return "Add Task Failed : a task with the same name already exists";
		else if ( foundList ==0)	return "Add Task Failed : List specified  doesnt exists";
		
		else {
			if ( estimatedTime >0 && priority >0) {
					
					Task task = new Task (list,name, estimatedTime, priority, description);
					
					taskList.add(task);
					msg="AddTask-Success";
					fichier.write(msg);
					return  msg; 
				}
			else
					return " Add Task failed : the estimated and priority time should be positive";
		}
	}
	
	
	
	//------------------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * 
	 * @param taskName represent the name of the task
	 * @return a String that indicate the status of the process Success or fail
	 */
	public String deleteTask (String taskName) {
		
		/*
		 * Check First if the task name exists
		 * if found then it deletes it and return a message of success 
		 * else a return of deletion failed
		 */
		int del=1;
		Iterator<Task> taskIterator = taskList.iterator();
		
		while (taskIterator.hasNext()) {
			if (taskIterator.next().getName().equals(taskName)) {
				taskIterator.remove();
				del=1;
			}
		}
		if (del ==1) {
			msg= "DeleteTask-Success";
			fichier.write(msg);
			return msg;
		}
		else
			return "Delete task failed : the task does not exist in the system" ;
		}
	
	
	//------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * 
	 * @param task : a String that represent the name of the task
	 * @param estimatedTime an integer representing the new estimated time to complete the task
	 * @param priority : is an Integer representing the new priority of the task
	 * @param description is a String representing the new description of that task
	 * @return a String defining the status of the process Success-fail
	 */
	public String editTask(String task, int estimatedTime, int priority, String description) {
		
		/*
		 * Check First if the task name exists
		 * if found then it modifies all his  properties with the ones
		 * passed by parameter 
		 * else return  edit  failed
		 */
		int found=0;
		Iterator<Task> taskIterator = taskList.iterator();
		
		while(taskIterator.hasNext()) {
			Task t = taskIterator.next();
			
			if (t.getName().equals(task)) {
				found=1;
				t.setName(task);
				t.setEstimatedTime(estimatedTime);
				t.setDescription(description);
				t.setPriority(priority);
			}		
		}
		if (found == 1) {
			msg="EditTask-Success";
			fichier.write(msg);
			return msg;
		}
		else
			return "Edit Task Failed : the task specified doesnt exist";
	}

	
	//------------------------------------------------------------------------------------------------------------------------------------------------
 /**
  * 
  * @param task : a String defining the name of the task
  * @param user : a String representing the name of the user we want to assign the task
  * @return a String that defines the satus of the process either succeeded or failed
  */
	public String assignTask(String task, String user) {
		/*
		 * First we check if the user and Task exist 
		 * if yes , we will add it a map , but first we go through verification
		 * if user dont exist  in the map, we add the user and task in a map
		 * else , we overwrite we update the tasks   
		 * & finally return either a success or failed message
		 */
		
		// check if the user exist
		int foundUser=0,foundTask=0;
		Iterator<User> userIterator=userList.iterator();
		Iterator<Task> taskIterator=taskList.iterator();
		
			while (userIterator.hasNext()) {
				if (userIterator.next().getName().equals(user))
					foundUser=1;
			}
			
			while (taskIterator.hasNext()) {
				
				if (taskIterator.next().getName().equals(task)) {
					foundTask=1;
				}
			}
				
				if (foundTask == 0)
						return "assign Task failed - task doesnt exist";
				
				else if (foundUser==0)
					return "assign task failed : user doesnt exist";
				
				else {
						/*
						 * a list containnig all the tasks of a user
						 */
						List<String> liste= new ArrayList<String>();
						
						if (! usersTasks.containsKey(user)) { //  if the user is not in the set
							
							liste.add(task);
							usersTasks.put(user, liste);
						}
						else {
							
							// Copy the list and add another methods
							Collections.copy(liste,usersTasks.get(user));
							liste.add(task);
							
							usersTasks.put(user, liste);
						}
						msg="Assigned to "+user;
						fichier.write(msg);
						return msg;
				}
		
	}
	

	//------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * 
	 * @param task a String representing the name of the task
	 * @return a String that contains all all the infos about that task
	 */
	public String printTask(String task) {
		/*
		 * 1-Check first if the task exist 
		 * if yes, then return all the messages of the Task
		 * else return a task does not exist message
		 */
		
		int found=0;
		StringBuilder sb = new StringBuilder();
		
		Iterator<Task> tasksIterator = taskList.iterator();
		
		while( tasksIterator.hasNext()) {
			Task t = tasksIterator.next();
			if (t.getName().equals(task)) {
				found=1;
				sb.append(t.toString());
			}			
		}
		if (found == 0)
				return ("The task doesnt Exist" ); 
		else {
			
			fichier.write(sb.toString());
			return sb.toString();
		}
		
		
	}
	
	
	//------------------------------------------------------------------------------------------------------------------------------------------------

	
	/**
	 * 
	 * @param task a String defining the name of the task 
	 * @return a String representing the status of that process
	 */
	public String completeTask(String task) {
		
		/*
		 * we first check if the task is in the TaksList
		 * if yes we add it to another list called completedTask & delete it from the taskList
		 * 
		 */
		
		int found=0;
		Iterator<Task> tasksIterator = taskList.iterator();
		while( tasksIterator.hasNext()) {
			Task t = tasksIterator.next();
			if (t.getName().equals(task)) {
				found = 1;
				completedTask.add(t);
				tasksIterator.remove();
			}
		}
		if (found ==1) {
			
				msg="Task Completed Successfully";
				fichier.write(msg);
				return msg;
		}
		else
			return "Task doesn't exist";
	}

	
	//------------------------------------------------------------------------------------------------------------------------------------------------

	
	/**
	 *  
	 * @return a String containing all the unassigned Priority in a
	 *  descending order based on their priorities
	 */
	
	public String printUnassignedTasksByPriority() {
		
		StringBuilder sBuilder = new StringBuilder();
		// retrieve all the unassigned tasks & save it 
		// into another list<Task> call unasignedTasks
		
			for (Task t: taskList) { // list of all the tasks
				// check if the task exists in the list of Task
				if (! usersTasks.values().contains(t.getName())) { 
					unassignedTask.add(t);
				}
			}
			
			// now we should sort the unassigned task list based on their priorities
			Collections.sort(unassignedTask,new Comparator<Task>() {
				//@Override 
				public int compare ( Task t1, Task t2) {
					return Integer.valueOf(t1.getPriority()).compareTo(t2.getPriority());
				}
			}
				
			);
			
			// build the String that should be return 
			for (Task t: unassignedTask) {
				// Priority
				sBuilder.append(t.getPriority());
				sBuilder.append(" | ");
				// Description
				sBuilder.append(t.getDescription());
				sBuilder.append(" | ");
				// User
				sBuilder.append("Unassigned");
				sBuilder.append(" | ");
				// Estimated Time
				sBuilder.append(t.getEstimatedTime() + "h\n");
			}
			if (!taskList.isEmpty()) {
				
				fichier.write(sBuilder.toString());
				return sBuilder.toString();
			}else
				return " There is no task yet";
	}

	
	//------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * 
	 * @param task : a String that is the name of the task
	 * @param list : a String that is the name of the list
	 * @return String that defines the status of the process succeeded or failed
	 */
	public String moveTask(String task, String list) {
		/*
		 * check if task & list exist
		 * if so , move task in the new List 
		 * and delete it from the old one
		 * 
		 */
		int existList=0, existTask=0;
		String resultat="";
		for (Task t: taskList) {
			
			if (t.getName().equals(task)) {
				existTask=1;
			}
		}
		for (Lists l : listOfLists) {
				
			if (l.getListsName().equals(list)) {
				
					existList=1;
			}
		}
		
		if (existList==1 && existTask==1) {
			
			for (Task t: taskList) {
				if (t.getName().equals(task)) {
					 resultat = editTask(task, t.getEstimatedTime(), t.getPriority(), t.getDescription());
				}
			}
			
			if (resultat.equals("EditTask-Success")) {
				msg="MoveTask-Success";
				fichier.write(msg);
			
				return msg;
			}
			else
				return resultat;
		}
		else
			return "Move task Failed";
	}

	//------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * 
	 * @param user a String representing the name of the user
	 * @return a String representing the satus of the process 
	 * Success or Failure
	 */
	public String printUserTasks(String user) {
		/*
		 * if user exists then return all his tasks
		 * else
		 * return user doesnt exist
		 */
		StringBuilder sb = new StringBuilder();
		
		if ( usersTasks.keySet().contains(user)) {
			
			for (String ta : usersTasks.get(user)) {
				sb.append(ta + "| ");
			}
			
			fichier.write(sb.toString());
			return sb.toString();
		}
		else {
					
			return " the user doesn't exist ";
	}

	}
	//------------------------------------------------------------------------------------------------------------------------------------------------

}
