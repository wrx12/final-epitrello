package fr.epita.finalProject.dataModel;

/**
 * Datamodel Class: Represents a Task entity of the Task Manager.
 * @author Emmanuel Bentum & Fabrice Prince
 */
public class Task {
	/*
	 * ########### CLASS ATTRIBUTES ###########
	 */
	/**
	 * A String representing the 'list' to which belongs a Task.
	 */
	private String list;
	/**
	 * Attribute: A String representing the 'name' of a Task.
	 */
	private String name;
	/**
	 * An integer value representing the 'estimated time' to complete a Task.
	 */
	private int    estimatedTime; 
	/**
	 * An integer value representing the 'priority' of a Task.
	 */
	private int    priority;
	/**
	 * A String representing the 'description' of a Task.
	 */
	private String description;
	
	/*
	 * ############ CONSTRUCTOR ############
	 */
	/**
	 * Constructor: Creates an instance of 'Task'
	 * @param list          A String representing the 'list' to which belongs a Task.
	 * @param name          A String representing the 'name' of a Task.
	 * @param estimatedTime An integer value representing the 'estimated time' to complete a Task.
	 * @param priority      An integer value representing the 'priority' of a Task.
	 * @param description	A String representing the 'description' of a Task.
	 */
	public Task (String list, String name,  int estimatedTime, int priority, String description) {
		this.list          = list;
		this.description   = description;
		this.estimatedTime = estimatedTime;
		this.priority      = priority;
		this.name          = name;
	}

	/*
	 * ############ GETTERS AND SETTERS ############
	 */
	/**
	 * Getter: Returns the attribute 'list' of a Task
	 * @return A String representing the 'list' to which belongs an instance of 'Task'
	 */
	public String getList() {
		return list;
	}
	
	/**
	 * Setter: Updates the attribute 'list' of a Task
	 * @param list A String representing the 'list' to which belongs an instance of 'Task'
	 */
	public void setList(String list) {
		this.list = list;
	}
	
	/**
	 * Getter: Returns the attribute 'name' of a Task
	 * @return A String representing the 'name' of a 'Task'
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter: Updates the attribute 'name' of a Task
	 * @param name A String representing the 'name' of a 'Task'
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Getter: Returns the attribute 'estimatedTime' of a Task
	 * @return An integer value representing the 'estimated time' to complete a 'Task'
	 */
	public int getEstimatedTime() {
		return estimatedTime;
	}
	
	/**
	 * Setter: Updates the attribute 'estimatedTime' of a Task
	 * @param estimatedTime An integer value representing the 'estimated time' to complete a 'Task'
	 */
	public void setEstimatedTime(int estimatedTime) {
		this.estimatedTime = estimatedTime;
	}
	
	/**
	 * Getter: Returns the attribute 'priority' of a Task
	 * @return An integer value representing the 'priority' of a 'Task'
	 */
	public int getPriority() {
		return priority;
	}
	
	/**
	 * Setter: Updates the attribute 'priority' of a Task
	 * @param priority An integer value representing the 'priority' of a 'Task'
	 */
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	/**
	 * Getter: Returns the attribute 'description' of a Task
	 * @return A String representing the 'description' of a 'Task'
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Setter: Updates the attribute 'description' of a Task
	 * @param description A String representing the 'description' of a 'Task'
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	/*
	 * ############ METHODS ############
	 */
	/**
	 * Method: Formats and returns a String describing the Task.
	 */
	public String toString() {
		return name + "\n"
				+ description +"\n"
				+ "Priority: " + priority +"\n" 
				+ "Estimated Time: "+ estimatedTime +"\n";
	}	
}
