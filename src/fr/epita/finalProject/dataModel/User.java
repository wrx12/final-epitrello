package fr.epita.finalProject.dataModel;

/**
 * Datamodel Class: Represents a User entity of the Task Manager.
 * @author Emmanuel Bentum & Fabrice Prince
 */
public class User {
	/*
	 * ########### CLASS ATTRIBUTES ###########
	 */
	/**
	 * Attribute: A String representing the 'name' of a 'User'.
	 */
	private String name;
	
	/*
	 * ############ CONSTRUCTOR ############
	 */
	/**
	 * Constructor: Creates an instance of 'User'
	 * @param name  A String representing the 'name' of a User.
	 */
	public User( String name) {
		this.name = name;
	}
	
	/*
	 * ############ GETTERS AND SETTERS ############
	 */
	/**
	 * Getter: Returns the attribute 'name' of a User
	 * @return A String representing the 'name' of a User
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter: Updates the attribute 'name' of a User
	 * @param name A String representing the 'name' of a User
	 */
	public void setName(String name) {
		this.name = name;
	}
}
