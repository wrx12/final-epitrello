package fr.epita.finalProject.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import fr.epita.finalProject.dataModel.User;

/**
 * Service Class: Manages access to the H2 Database.
 * @author Emmanuel Bentum & Fabrice Prince
 */
public class DatabaseDAO {
	/*
	 * ############ CONSTRUCTOR ############
	 */
	/**
	 * Constructor: Creates an instance of 'DatabaseDAO'
	 */
	public DatabaseDAO () {}
	
	/*
	 * ############ METHODS ############
	 */
	/**
	 * Method: Allows connection to the H2 database.
	 * <p>
	 * Requires:
	 * 	<ul>
	 * 		<li>url:      Path to the database.</li>
	 * 		<li>username: Database's user name.</li>
	 *  	<li>password: User's password .</li>
	 *  </ul>
	 * </p>
	 * @return Connection
	 * @throws SQLException
	 * @throws ClassNotFoundException 
	 */
	public static Connection getConnection () throws SQLException, ClassNotFoundException {
		// Path to the database
		
		String url = "jdbc:h2:tcp://localhost/~/git/final-epitrello/database/epitrelloDB";
		Class.forName("org.h2.Driver");
		// Username
		String username = "sa";
		// Password
		String password = "";
		// Establish the connection to DB
		Connection connection = DriverManager.getConnection(url, username, password);
		
		return connection;
	}
	
	/**
	 * Method: Allows to create a {@code USER} table into the DB.
	 * @return A boolean value describing the exit status of the {@code CREATE TABLE} operation.
	 * @throws ClassNotFoundException
	 */
	public boolean createTable () throws ClassNotFoundException {
		// Connect to the DB and create a table USER
		try (Connection connection = DatabaseDAO.getConnection()) {
			PreparedStatement prepareStatement = connection.prepareStatement("CREATE TABLE USERS(ID INT AUTO_INCREMENT PRIMARY KEY, NAME VARCHAR(500))");
			// Returns the exit status of table creation in the DB
			return prepareStatement.execute();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Method: Allows to insert a User into the DB.
	 * @param  user An instance of a 'User'.
	 * @return An integer value describing the exit status of the 'INSERT' operation.
	 * @throws ClassNotFoundException
	 */
	public int createUser (User user) throws ClassNotFoundException {
		// Connect to the DB and create an entry in table USER
		try (Connection connection = DatabaseDAO.getConnection()) {
			PreparedStatement prepareStatement = connection.prepareStatement("INSERT INTO USERS (NAME) VALUES(?)");	
			//PreparedStatement prepareStatement = connection.prepareStatement("INSERT IGNORE INTO users (id,name) VALUES (?,?) VALUES(?,?) ");  
			prepareStatement.setString(1, user.getName());
			// Returns the exit status of inserting into the DB
			return prepareStatement.executeUpdate();
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	/**
	 * Method: Allows to search the database and retrieve all Users.
	 * @return A List of User instances.
	 * @throws ClassNotFoundException 
	 */
	public ArrayList<User> searchUsers () throws ClassNotFoundException {
		// Connect to the DB and collect all content from table USER
		try (Connection connection = DatabaseDAO.getConnection()) {
			PreparedStatement prepareStatement = connection.prepareStatement("SELECT * FROM USERS ORDER BY NAME");
			ResultSet results = prepareStatement.executeQuery();
			
			// Prepare the list of Users to return
			ArrayList<User> users = new ArrayList<>();
			
			// Create an instance of User for each entry retrieved
			while (results.next()) {
				String name = results.getString("NAME");
				User user = new User(name);
				users.add(user);
			}
			// Return the list of users 
			return users;	
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		// return an empty List if otherwise
		return new ArrayList<>();
	}
}
