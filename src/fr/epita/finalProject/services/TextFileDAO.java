package fr.epita.finalProject.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class TextFileDAO {

	
	public TextFileDAO() {
		
	}
	
	public void write(String msg) {
		
		
		File file = new File("output.txt");
		
		try {
			
			if (!file.exists()){
				file.createNewFile();
			}
			// append mode
			FileWriter fileWriter = new FileWriter(file, true);
			PrintWriter writer = new PrintWriter(fileWriter, true);
			
			writer.println(msg);
			
			writer.close();
			System.out.println("saved ! ");
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
}
