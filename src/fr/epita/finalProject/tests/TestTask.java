package fr.epita.finalProject.tests;

import fr.epita.finalProject.dataModel.Lists;
import fr.epita.finalProject.dataModel.Task;

/**
 * Test Class: Performs testing on {@code Task} class.
 * @author Emmanuel Bentum & Fabrice Prince
 */
public class TestTask {
	/**
	 * <p>Main method: 
	 * Test the following operations:
	 * 	<ul>
	 * 		<li>Create a {@code Task}</li>
	 * 		<li>Display the {@code Task}</li>
	 * 		<li>Update the {@code Task} attributes</li>
	 * 	</ul>
	 * </p>
	 * @param args Command line arguments of the {@code main} method
	 */
	public static void main(String[] args) {
		Task task = new Task("Grocery Shopping", "Buy Pork Meat", 1, 24, "Get a restock of pork meat for next week meals");
		
		System.out.println("Succesfully created Task\n" +task);
		
		System.out.println("Changing Task to Java Programming...");
		task.setName("Java Programming");
		task.setList("Studying");
		task.setPriority(1);
		task.setDescription("Java Practical Exam has been annonced. Get some practice before D-day!");
		task.setEstimatedTime(48);
		
		System.out.println("Task is now\n"+task);
	}

}