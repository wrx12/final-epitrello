package fr.epita.finalProject.tests;

import fr.epita.finalProject.services.DatabaseDAO;
import fr.epita.finalProject.dataModel.User;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;

/**
 * Test Class: Performs testing on Database access operations.
 * @author Emmanuel Bentum & Fabrice Prince
 */
public class TestDatabase {
	/**
	 * <p>Main method: 
	 * Test the following operations:
	 * 	<ul>
	 * 		<li>Connect to the DB</li>
	 * 		<li>Insert a User into DB</li>
	 * 		<li>Retrieve Users from DB</li>
	 * 	</ul>
	 * </p>
	 * @param args Command line arguments of the {@code main} method
	 * @throws SQLException 
	 * @throws ClassNotFoundException
	 */
	public static void main(String[] args) throws SQLException, ClassNotFoundException {	
		// Create a User Toto
		User user = new User("Toto");
		
		// Establish connection to/Create DB
		System.out.println("Connecting to Database...");
		// Instantiate a UserDAO
		DatabaseDAO userDao = new DatabaseDAO();
		
		// Insert a User Toto into DB
		System.out.println("Insertion of User " + user.getName() + "...");
		int exitStatus = userDao.createUser(user);
		
		// Show success or failure insertion
		boolean success = exitStatus != 0;
		System.out.println("Insertion Success ? : " + success);
		
		// Create 2 more Users, Insert, and Retrieve them all from DB
		User user1 = new User("John");
		User user2 = new User("Jane");
		
		System.out.println("\nInsertion of Users " + user1.getName() + " and " + user2.getName() + "...");
		exitStatus = userDao.createUser(user1);
		System.out.println("Insertion Success ? : " + success);
		exitStatus = userDao.createUser(user2);
		System.out.println("Insertion Success ? : " + success);
		// Newline
		System.out.println();
		
		List<User> users = userDao.searchUsers();
		Iterator<User> userListIterator = users.iterator();
		
		// Display the retrieved Users names
		while(userListIterator.hasNext()) {
			System.out.println("User " + userListIterator.next().getName() + " retrieved !");
		}
	}
}
