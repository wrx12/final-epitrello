package fr.epita.finalProject.tests;

import fr.epita.finalProject.dataModel.Lists;

/**
 * Test Class: Performs testing on {@code Lists} class.
 * @author Emmanuel Bentum & Fabrice Prince
 */
public class TestLists {
	/**
	 * <p>Main method: 
	 * Test the following operations:
	 * 	<ul>
	 * 		<li>Create a {@code Lists}</li>
	 * 		<li>Print the {@code Lists} name</li>
	 * 		<li>Update {@code Lists} name</li>
	 * 	</ul>
	 * </p>
	 * @param args Command line arguments of the {@code main} method
	 */
	public static void main(String[] args) {
		Lists list = new Lists("Liste1");
		
		System.out.println("Successfully created List " +list.getListsName());
		
		System.out.println("Changing List name to TheUltimateList...");
		list.setListsName("TheUltimateList");
		
		System.out.println("List is now " +list.getListsName());
	}
}