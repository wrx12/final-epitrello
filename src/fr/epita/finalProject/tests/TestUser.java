package fr.epita.finalProject.tests;

import fr.epita.finalProject.dataModel.User;

/**
 * Test Class: Performs testing on {@code User} class.
 * @author Emmanuel Bentum & Fabrice Prince
 */
public class TestUser {
	/**
	 * <p>Main method: 
	 * Test the following operations:
	 * 	<ul>
	 * 		<li>Create a {@code User}</li>
	 * 		<li>Print the {@code User} name</li>
	 * 		<li>Update {@code User} name</li>
	 * 	</ul>
	 * </p>
	 * @param args Command line arguments of the {@code main} method
	 */
	public static void main(String[] args) {
		User user = new User("Toto");
		
		System.out.println("Successfully created User " +user.getName());
		
		System.out.println("Changing User's name to Mimi...");
		user.setName("Mimi");
		
		System.out.println("User is now " +user.getName());
	}
}
